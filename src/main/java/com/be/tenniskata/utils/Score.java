package com.be.tenniskata.utils;

public enum Score {

	LOVE(0,0),
	FIFTEEN(1,15),
	THIRTY(2,30),
	FORTY(3,40);
	
	private final int index;
	private final int score;
	
	Score(int index, int score){
		this.index = index;
		this.score = score;
	}

	public int getIndex(){
		return index;
	}
	public int getScore(){
		return score;
	}
	public int getScorebyIndex(int index){
		if(LOVE.getIndex() == index){
			return LOVE.getScore();
		}else if(FIFTEEN.getIndex() == index){
			return FIFTEEN.getScore();
		}else if(THIRTY.getIndex() == index){
			return THIRTY.getScore();
		}else if(FORTY.getIndex() == index){
			return FORTY.getScore();
		}
		return 0;
	}
}
