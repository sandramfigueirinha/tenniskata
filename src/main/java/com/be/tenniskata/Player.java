package com.be.tenniskata;
/**
 * The class Player. 
 */
public class Player {
	private int totalScore;
	private String name;
	private boolean isInAdvantage;
	
	public Player(String name){
		setName(name);
	}

	/**
	 * Increases the score of a player that won the set.
	 */
	public void score(){
		totalScore++;
	}

	/**
	 * Gets the totalScore.
	 * @return the totalScore
	 */
	public int getTotalScore() {
		return totalScore;
	}

	/**
	 * Sets the TotalScore-
	 * @param totalScore the totalScore to set
	 */
	public void setTotalScore(int totalScore) {
		this.totalScore = totalScore;
	}

	/**
	 * Gets the name.
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * Gets the isINAdvantage.
	 * @return the isInAdvantage
	 */
	public boolean isInAdvantage() {
		return isInAdvantage;
	}
	/**
	 * Sets the isInAdvantage.
	 * @param isInAdvantage the isInAdvantage to set
	 */
	public void setInAdvantage(boolean isInAdvantage) {
		this.isInAdvantage = isInAdvantage;
	}

}
