package com.be.tenniskata;

public class App {

	public static void main(String[] args) {
		
		//create a game
		Game game = new Game();
		//create 2 players and set a name
		Player player1 = new Player("player 1");
		Player player2 = new Player("player 2");
		//play tennis
		game.playTennis(player1, player2);
		
	}

}
