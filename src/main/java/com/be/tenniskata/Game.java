package com.be.tenniskata;

import java.util.concurrent.ThreadLocalRandom;

import com.be.tenniskata.utils.GameConstants;
import com.be.tenniskata.utils.Score;


public class Game {
	private String status="";
	private Player winnerPlayer;
	public Game(){

	}
	public void playTennis(Player player1, Player player2){
		boolean winner= false;
		int i = 1;
		while(!winner){
			System.out.println("******playing tennis round " + i + "******");
			//choose randomly the player that will score
			int scored = choosePlayer();
			if(scored == 0){
				winner = score(player1, player2, winner);

			}else{
				winner = score(player2, player1, winner);
			}
			i++;
		}
	}
	public boolean score(Player scoredRoundPlayer, Player notScoredSetPlayer, boolean winner){

		System.out.println("Winner of this round" + scoredRoundPlayer.getName());
		scoredRoundPlayer.score();
		if(GameConstants.STATUS_ADVANTAGE.equals(getStatus())){
			if(scoredRoundPlayer.isInAdvantage()){
				winner = true;
				System.out.println("*********Game is Over**********");
				System.out.println("Winner Player("+ scoredRoundPlayer.getName() + ") win after Advantage with a score of " + scoredRoundPlayer.getTotalScore());
				System.out.println("Score of the second player(" + notScoredSetPlayer.getName() + ") Points: "+ notScoredSetPlayer.getTotalScore());
				winnerPlayer = scoredRoundPlayer;
				return winner;
			}else{
				setStatus(GameConstants.STATUS_DEUCE);
				notScoredSetPlayer.setInAdvantage(false);
				System.out.println("Setting status Deuce");
			}
		}else if(GameConstants.STATUS_DEUCE.equals(getStatus())){
			scoredRoundPlayer.setInAdvantage(true);
			setStatus(GameConstants.STATUS_ADVANTAGE);
			System.out.println("Setting status advantage to player: " + scoredRoundPlayer.getName());
		} else{
			//A game is won by the first player to have won at least four points in total and at least two points more than the opponent.
			if(scoredRoundPlayer.getTotalScore()> 3 && notScoredSetPlayer.getTotalScore() < 3){
				winner = true;
				System.out.println("*********Game is Over**********");
				System.out.println("Winner Player("+ scoredRoundPlayer.getName() + ") with a score of " + scoredRoundPlayer.getTotalScore());
				System.out.println("Score of the second player(" + notScoredSetPlayer.getName() + ") Points: "+ notScoredSetPlayer.getTotalScore());
				winnerPlayer = scoredRoundPlayer;
				return winner;
			}

			//If both have 40 the players are deuce.
			if(scoredRoundPlayer.getTotalScore() >= 3 && scoredRoundPlayer.getTotalScore()==notScoredSetPlayer.getTotalScore()){
				setStatus(GameConstants.STATUS_DEUCE);
				System.out.println("Status of the game: DEUCE");
			}
		}

		return winner;
	}

	public int choosePlayer(){
		// nextInt is normally exclusive of the top value,
		// so add 1 to make it inclusive
		int randomNum = ThreadLocalRandom.current().nextInt(0, 1 + 1);
		return randomNum;
	}
	/**
	 * Gets the status of the game
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * Sets the status of the game.
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * Gets the score of the match
	 * @param score The score
	 * @return the name of the score.
	 */
	public int getScoreOfMatch(int score){
		Score[] values = Score.values();
		for(Score value: values){
			if (value.getIndex()==score){
				return value.getScore();
			}
		}
		return -1;
	}
	/**
	 * @return the winnerPlayer
	 */
	public Player getWinnerPlayer() {
		return winnerPlayer;
	}
	/**
	 * @param winnerPlayer the winnerPlayer to set
	 */
	public void setWinnerPlayer(Player winnerPlayer) {
		this.winnerPlayer = winnerPlayer;
	}
}
