package com.be.tenniskata;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.be.tenniskata.utils.GameConstants;

public class GameTest {
	Game game;
	Player player1;
	Player player2;
	@Before
	public void setUp() throws Exception {
		game = new Game();
		player1= new Player("player 1");
		player2 = new Player("player 2");
	}

	@Test
	public void testWinPlayer1() {

		player1.score();
		player1.score();
		player2.score();
		player1.score();
		player1.score();
		
		game.play(player1, player2, false);
		Assert.assertEquals(player1.getName(),game.getWinnerPlayer().getName());
	}

	@Test
	public void testWinPlayer2() {

		player1.score();
		player1.score();
		player2.score();
		player2.score();
		player2.score();
		player2.score();
		
		game.play(player2, player1, false);
		Assert.assertEquals(player2.getName(),game.getWinnerPlayer().getName());
	}
	
	@Test
	public void testAdvantage(){
		player1.setInAdvantage(true);

		game.setStatus(GameConstants.STATUS_ADVANTAGE);
		
		game.play(player1, player2, false);
		Assert.assertEquals(player1.getName(),game.getWinnerPlayer().getName());

		
	}
}
